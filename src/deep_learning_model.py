import os
import re

import keras
import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from spacy.compat import pickle
from tensorflow.python.keras import Model, Input
from tensorflow.python.keras.layers import Embedding, Dense, GlobalMaxPooling1D, MaxPooling1D, Dropout, Conv1D

NB_WORDS = 20000
MAX_LENGTH=150


def load_data():
    df = pd.read_csv(os.getcwd() + '/dataset/IMDB Dataset.csv')
    df = df[['review', 'sentiment']]
    df['review'] = df['review'].str.lower()
    df.review = df.review.apply(remove_stopwords).apply(remove_mentions)
    return df


def remove_stopwords(input_text):
    stopwords_list = stopwords.words('english')
    whitelist = ["n't", "not", "no"]
    words = input_text.split()
    clean_words = [word for word in words if (word not in stopwords_list or word in whitelist) and len(word) > 1]
    return " ".join(clean_words)


def remove_mentions(input_text):
    clean_text = re.sub(r'@\w+', '', input_text)
    clean_text = re.sub('https?://[A-Za-z0-9./]+', '', clean_text)
    clean_text = re.sub("[^a-zA-Z]", " ", clean_text)
    return clean_text


df = load_data()
reviews = df.review
reviews_list = []
for i in range(len(reviews)):
    reviews_list.append(reviews[i])
y = np.array(list(map(lambda x: 1 if x == "positive" else 0, df.sentiment)))
X_train, X_test, Y_train, Y_test = train_test_split(reviews_list, y, test_size=0.2, random_state=45)

tk = Tokenizer(num_words=NB_WORDS,
               filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n',
               lower=True,
               split=" ")
tk.fit_on_texts(X_train)
words_to_index = tk.word_index


def read_glove_vector(glove_vec):
    with open(glove_vec, 'r', encoding='UTF-8') as f:
        word_to_vec_map = {}
        for line in f:
            w_line = line.split(' ')
            curr_word = w_line[0]
            word_to_vec_map[curr_word] = np.array(w_line[1:], "float32")
    return word_to_vec_map


word_to_vec_map = read_glove_vector(os.getcwd() + '/glove model/glove.840B.300d.txt')

vocab_len = len(words_to_index) + 1
embed_vector_len = word_to_vec_map['moon'].shape[0]
emb_matrix = np.zeros((vocab_len, embed_vector_len))

for word, index in words_to_index.items():
    embedding_vector = word_to_vec_map.get(word)
    if embedding_vector is not None:
        emb_matrix[index, :] = embedding_vector

embedding_layer = Embedding(input_dim=vocab_len, output_dim=embed_vector_len,
                            input_length=MAX_LENGTH, weights=[emb_matrix], trainable=False)


def construct_model(input_shape):
    X_indices = Input(input_shape)
    embeddings = embedding_layer(X_indices)
    X = Conv1D(512, 3, activation='relu')(embeddings)
    X = MaxPooling1D(3)(X)
    X = Conv1D(256, 3, activation='relu')(X)
    X = MaxPooling1D(3)(X)
    X = Conv1D(256, 3, activation='relu')(X)
    X = Dropout(0.8)(X)
    X = MaxPooling1D(3)(X)
    X = GlobalMaxPooling1D()(X)
    X = Dense(256, activation='relu')(X)
    X = Dense(1, activation='sigmoid')(X)
    model = Model(inputs=X_indices, outputs=X)
    return model


model = construct_model((MAX_LENGTH,))
model.summary()

X_train_indices = tk.texts_to_sequences(X_train)
X_train_indices = pad_sequences(X_train_indices, maxlen=MAX_LENGTH, padding='post')

adam = keras.optimizers.Adam(learning_rate=0.0001)
model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
model.fit(X_train_indices, Y_train, batch_size=64, epochs=15)

X_test_indices = tk.texts_to_sequences(X_test)
X_test_indices = pad_sequences(X_test_indices, maxlen=MAX_LENGTH, padding='post')

model.evaluate(X_test_indices, Y_test)

open(os.getcwd() + '/pre_trained model/model_config.json', 'w').write(model.to_json())
model.save_weights(os.getcwd() + '/pre_trained model/weights.h5', overwrite=True)
with open(os.getcwd() + '/pre_trained model/tokenizer.pickle', 'wb') as handle:
    pickle.dump(tk, handle, protocol=pickle.HIGHEST_PROTOCOL)
