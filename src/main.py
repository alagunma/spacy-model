import uvicorn
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from src import create_app
from src.Sentiment import GetSentiment

app = create_app()

from pydantic import BaseModel


class Topic(BaseModel):
    text: str
    sentiment: str

@app.post('/analyse', )
async def predict_sentiment(topic:Topic):
    print(topic)
    formatted_topic = str(topic.text).replace('\n', ' ')
    return JSONResponse(jsonable_encoder(GetSentiment(formatted_topic)))


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8080, log_level="info")