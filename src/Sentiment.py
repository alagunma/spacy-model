import cytoolz
import os
import re

import keras
from keras_preprocessing.sequence import pad_sequences

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from tensorflow.keras.models import model_from_json
from spacy.compat import pickle
import spacy, string

DATASET_ENCODING = "ISO-8859-1"
USERNAME_CLEANING_RE = "@[A-z-0-9]*"
LINK_CLEANING_RE = "(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"
translator = str.maketrans('', '', string.punctuation)


class SentimentAnalyser(object):
    @classmethod
    def load(cls, nlp, max_length=150):
        model = model_from_json(open(os.getcwd() + "/pre_trained model/model_config.json").read())
        model.load_weights(os.getcwd() + "/pre_trained model/weights.h5")
        adam = keras.optimizers.Adam(learning_rate=0.0001)
        model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
        return cls(model, max_length=max_length)

    def __init__(self, model, max_length=150):
        self._model = model
        self.max_length = max_length

    def __call__(self, doc):
        X = get_features([doc], self.max_length)
        y = self._model.predict(X)
        self.set_sentiment(doc, y)

    def pipe(self, docs, batch_size=1000):
        for minibatch in cytoolz.partition_all(batch_size, docs):
            minibatch = list(minibatch)
            sentences = []
            doc_list = []
            for doc in minibatch:
                sentences.extend(doc.sents)
                doc_list.append(doc.text)
            Xs = get_features(doc_list, self.max_length)
            ys = self._model.predict(Xs)
            for sent, label in zip(sentences, ys):
                sent.doc.sentiment = label
            for doc in minibatch:
                yield doc

    def set_sentiment(self, doc, y):
        doc.sentiment = float(y[0])


def get_features(docs, max_length):
    with open(os.getcwd() + '/pre_trained model/tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    reviews_list_tk = tokenizer.texts_to_sequences(docs)
    reviews_list_idx = pad_sequences(reviews_list_tk, maxlen=max_length, padding='post')
    return reviews_list_idx


def preprocess(text, stem=False):
    text = re.sub(USERNAME_CLEANING_RE, '<USER>', str(text).strip())
    text = re.sub(LINK_CLEANING_RE, '<LINK>', str(text).strip())
    text = text.replace(".", " ").replace(",", " ").replace("?", " ").replace("!", "").replace("-", " ")
    return text.lower()


nlp = spacy.load('en_core_web_lg')
nlp.add_pipe(nlp.create_pipe("sentencizer"))
nlp.add_pipe(SentimentAnalyser.load(nlp, max_length=150))
print("################################################")
print("Sentiment model loaded successfully")
print("################################################")


def GetSentiment(text):
    text = "This is the best day ever"
    data = [preprocess(text)]
    result = [doc.sentiment for doc in nlp.pipe(data)]
    if result[0] > 0:
        resultjson = {'topic': text, 'accuracy': {'positive': (result[0] * 100),
                                                  'negative': (0 * 100)}}
    else:
        resultjson = {'topic': text, 'accuracy': {'positive': (0 * 100),
                                                  'negative': (result[0] * 100)}}
    return resultjson

