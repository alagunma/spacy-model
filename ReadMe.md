###Create a virtual environment using the following cmd:

`python3.7 -m venv (Virtual Environment name)`

###Run the following to activate the virtual environment:

`source (Virtual Environment name)/bin/activate`

###Download the resources in the req.txt using following cmd:

`pip3 install -r req.txt`

###Download the nlp model using:
`pip3 install https://github.com/explosion/spacy-models/releases/download/en_core_web_lg-2.3.1/en_core_web_lg-2.3.1.tar.gz`

###Navigate to main in the src directory and run the main.py

###Open the browser, Enter the URL `http://localhost:8080/docs`


###Reg keras model:
###To train/remodel keras model

###Download the `glove.840B.300d.txt` from Stanford NLU site into glove data set folder


